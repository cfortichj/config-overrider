import os
import unittest

from config_overrider.json.json_config_overrider import json_config_overrider
from tests import TEST_BASE


class TestJsonConfigOverrider(unittest.TestCase):

    def test_json_config_overrider(self):

        # arrange
        default_path = os.path.join(TEST_BASE, "resources/default")
        modified_path = os.path.join(TEST_BASE, "resources/modified")

        # act
        processed_output = json_config_overrider(default_path=default_path, modified_path=modified_path)

        expected_output = {
            "json1.json": {
                "key1.1": "modified_value_1",
                "key1.2": 3,
                "key1.3": True,
                "key1.4": None,
                "key1.5": 4.5
            },
            "json2.json": {
                "key2.1": "value2",
                "key2.2": 8.5,
                "key2.3": 4,
                "key2.4": False,
                "key2.5": None
            }
        }

        self.assertEquals(processed_output, expected_output)
        return
