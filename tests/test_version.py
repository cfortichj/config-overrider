import unittest

import config_overrider


class VersionTestCase(unittest.TestCase):
    """ Version json """

    def test_version(self):
        """ check config_overrider exposes a version attribute """
        self.assertTrue(hasattr(config_overrider, "__version__"))
        self.assertIsInstance(config_overrider.__version__, str)


if __name__ == "__main__":
    unittest.main()
