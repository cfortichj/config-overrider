User Guide
##########

This section of the documentation provides user focused information such as
installing and quickly using this package.

.. _install-guide-label:

Install Guide
=============

.. note::

    It is best practice to install Python projects in a virtual environment,
    which can be created and activated as follows using Python 3.6+.

    .. code-block:: console

        $ conda create --name config-overrider --python=3.10
        $ conda activate config-overrider
        (config-overrider) $

The simplest way to install ConfigOverrider is using Pip.

.. code-block:: console

    $ pip install config-overrider

This will install ``config-overrider`` and all of its dependencies.


.. _api-reference-label:


Report Bugs
===========

Report bugs at the `issue tracker <https://gitlab.com/opendatascientists/configoverrider/issues>`_.

Please include:

  - Operating system name and version.
  - Any details about your local setup that might be helpful in troubleshooting.
  - Detailed steps to reproduce the bug.
