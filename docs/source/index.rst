ConfigOverrider
###############

Override defaults from configuration files

.. toctree::
   :maxdepth: 2
   :numbered:
   :hidden:

   user/index
   api/index
   dev/index


Quick Start
===========

ConfigOverrider is available on PyPI and can be installed with `pip <https://pip.pypa.io>`_.

.. code-block:: console

    $ pip install config-overrider

After installing ConfigOverrider you can use it like any other Python module.

Here is a simple example:

.. code-block:: python

    import config_overrider
    # Fill this section in with the common use-case.
