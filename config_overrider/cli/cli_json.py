import click

from config_overrider.json.json_config_overrider import (
    dump_overriden_conf,
)

from config_overrider.json.save_config_overrider import (
    save_config_overrider,
)


@click.command()
@click.option("--default-path", "-d", help="Path to the default configuration folder")
@click.option("--modified-path", "-m", help="Path to the modified configuration folder")
def cli_dump_overriden_conf(default_path: str, modified_path: str) -> None:
    """
    CLI wrapper around func::`json`
    TODO: Docstring
    """
    dump_overriden_conf(default_path, modified_path)

    return None


@click.command()
@click.option("--default-path", "-d", help="Path to the default configuration folder")
@click.option("--modified-path", "-m", help="Path to the modified configuration folder")
@click.option("--output-path", "-o", help="Path to the output configuration folder")
def cli_save_config_overrider(default_path: str, modified_path: str, output_path: str) -> None:
    """
    CLI wrapper around func::`json`
    TODO: Docstring
    """

    save_config_overrider(default_path, modified_path, output_path)

    return None
