import json
import os

from config_overrider import logger
from config_overrider.json.json_config_overrider import json_config_overrider


def save_config_overrider(default_path: str, modified_path: str, output_path: str) -> None:
    """
    TODO: Docstring
    """

    configuration_dump = json_config_overrider(default_path=default_path, modified_path=modified_path)

    logger.debug(f"Configuration dump is {configuration_dump}")
    logger.info(f"Saving configuration at {output_path}")

    for file_name, conf_dump in configuration_dump.items():
        full_file_name = os.path.join(output_path, file_name)
        # open file in write mode
        with open(f"{full_file_name}", "w") as f_conf_save:
            logger.debug(f"Processing file {full_file_name}")
            json.dump(conf_dump, f_conf_save)

    logger.info("Finished writing files")
