import json
import os
from typing import (
    Any,
    Dict,
)

from config_overrider import logger


def dump_overriden_conf(default_path: str, modified_path: str) -> None:
    """
    TODO: Docstring
    Parameters
    ----------
    default_path
    modified_path

    Returns
    -------

    """

    conf = json_config_overrider(default_path=default_path, modified_path=modified_path)
    logger.info(conf)
    return


def json_config_overrider(default_path: str, modified_path: str) -> Dict[str, Dict[str, Any]]:
    """
    This function modifies the configuration present in default_path, changing the values of the keys for those present
    in modified_path and returning a dict whose keys are the names of the files and the value is another dict containing
     the JSON

    Parameters
    ----------
    - default_path: str: String containing the path of the default configuration files
    - modified_path: str: String containing the path of the modified keys

    Returns
    -------
    Dict[str, Dict[str, Any]]: Dict containing the name of the file and the dict with the modified defaults

    """

    pth_default_path = [f"{os.path.join(default_path, elem_path)}" for elem_path in os.listdir(default_path)]
    pth_modified_path = [f"{os.path.join(modified_path, elem_path)}" for elem_path in os.listdir(modified_path)]

    logger.debug(f"Elements in default path: {pth_default_path}")
    logger.debug(f"Elements in modified path: {pth_modified_path}")

    dict_modifications = {}
    defaults = {}

    logger.info("Reading modifications into a dict")
    # loading all modifications into a single dict, no need of separate them
    for elem_modifications in pth_modified_path:
        with open(elem_modifications) as f_modifications_json:
            modifications_json = json.load(f_modifications_json)

        dict_modifications = {**dict_modifications, **modifications_json}

    logger.info("Reading defaults into a dict and merging with modifications")
    # we would like to have separated defaults by their names
    for elem_defaults in pth_default_path:
        file_name = elem_defaults.split("/")[-1]

        logger.debug(f"Processing default file {file_name}")

        with open(elem_defaults) as f_defaults_json:

            # for defaults, we keep a dict of type {file_name -> dict}, because we want to track which dict maps to
            # which file
            inner_default_dict = json.load(f_defaults_json)

            # we keep the keys of the default dict...
            inner_default_dict_keys = inner_default_dict.keys()

            # ... because when mixing with the modified dict, we don't want to keep keys not present on the default dict
            inner_default_dict = {dict_key: dict_value
                                  for (dict_key, dict_value) in {**inner_default_dict, **dict_modifications}.items()
                                  if dict_key in inner_default_dict_keys
                                  }

            defaults_json = {file_name: inner_default_dict}

        defaults = {**defaults, **defaults_json}

    logger.info("Success reading configuration from JSON!")

    return defaults
