import logging
import sys

import click

from config_overrider import logger

from config_overrider.cli.cli_json import (
    cli_dump_overriden_conf,
    cli_save_config_overrider,
)


@click.group()
@click.option("--verbose", "-v", is_flag=True, default=False, type=click.BOOL, help="Whether to output verbose logs")
def main(verbose: bool) -> None:
    if verbose:
        logger.setLevel(logging.DEBUG)


@click.command()
def cli_test() -> None:
    """
    Function used to test click CLI
    """

    print("Click CLI is working!")

    return


main.add_command(cli_test, name="cli-test")
main.add_command(cli_dump_overriden_conf, name="dump-json-config")
main.add_command(cli_save_config_overrider, name="save-json-config")

if __name__ == "__main__":
    sys.exit(main())
