"""
Override defaults from configuration files
"""

__version__ = "0.0.1"
import logging

logging.basicConfig(format='%(asctime)s - %(module)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
