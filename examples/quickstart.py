"""
This example script imports the config_overrider package and
prints out the version.
"""

import config_overrider


def main():
    print(
        f"config_overrider version: {config_overrider.__version__}"
    )


if __name__ == "__main__":
    main()
